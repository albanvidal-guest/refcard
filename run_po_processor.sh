#!/bin/sh
# Copyright (C) 2016, Omer Zak.
# This program is free software, licensed under the GNU GPL, >=3.0.
# This software comes with absolutely NO WARRANTY. Use at your own risk!
for lang in ar fa he; do
  if [ -e po4a/${lang}.po ]; then
    ./po_processor.py  -o /tmp/${lang}.po.x < po4a/${lang}.po
    cp /tmp/${lang}.po.x po4a/${lang}.po
  fi
done
